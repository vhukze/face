
package com.vhukze.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vhukze.mapper.FaceMapper;
import com.vhukze.pojo.User;

/**
* @author zsz
* @version 
* @创建时间：2019年10月29日 下午5:07:52
*/
@Service
public class FaceService {
	
	@Autowired
	private FaceMapper mapper;

	//注册人脸
	public Boolean faceAdd(User user) {
		return mapper.add(user);
	}
	//查找人脸
	public List<User> getAllUserFace() {
		return mapper.getAllUser();
	}
}


