
package com.vhukze.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.vhukze.pojo.User;

/**
* @author zsz
* @version 
* @创建时间：2019年10月30日 上午10:27:54
*/
@Mapper
public interface FaceMapper {

	@Insert("insert into user_face values(#{username},#{extract})")
	Boolean add(User user);

	@Select("select * from user_face")
	List<User> getAllUser();
}


