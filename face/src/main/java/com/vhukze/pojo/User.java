
package com.vhukze.pojo;

/**
* @author zsz
* @version 
* @创建时间：2019年10月30日 上午10:53:16
*/
public class User {

	//用户名
	private String username;
	//用户人脸特征
	private byte[] extract;
	
	public User(String username, byte[] extract) {
		super();
		this.username = username;
		this.extract = extract;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public byte[] getExtract() {
		return extract;
	}
	public void setExtract(byte[] extract) {
		this.extract = extract;
	}
	
	
}


