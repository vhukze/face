
package com.vhukze.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
* @author zsz
* @version 
* @创建时间：2019年10月29日 下午4:13:28
*/
@SpringBootApplication
@ComponentScan(basePackages = "com.vhukze")
@MapperScan("com.vhukze.mapper")
public class FaceApp {

	public static void main(String[] args) {
		SpringApplication.run(FaceApp.class, args);
	}
}


