
package com.vhukze.app;

import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.enums.ErrorInfo;

/**
 * 设备引擎激活
* @author zsz
* @version 
* @创建时间：2019年10月29日 下午2:51:24
*/
public class FaceActivation{

	public static void main(String[] args) {
		String appId = "CHuRTesMsVrfR6pZwyQt7JMkB7BSR4P5QsBxneQvsNzR";
        String sdkKey = "3osgFgkF7ygzEuYnTTW99iUvks844N85f5j9Cgn6SKqm";

        FaceEngine faceEngine = new FaceEngine("D:\\人脸识别\\ArcSoft_ArcFace_Java_Windows_x64_V2.2\\libs\\WIN64");
        //激活引擎
        int activeCode = faceEngine.activeOnline(appId, sdkKey);
        

        if (activeCode != ErrorInfo.MOK.getValue() && activeCode != ErrorInfo.MERR_ASF_ALREADY_ACTIVATED.getValue()) {
            System.out.println("引擎激活失败");
        }else {
        	System.out.println("引擎激活成功");
        }
	}
}


