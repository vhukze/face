
package com.vhukze.face;

import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceSimilar;

/**
 * 人脸特征对比
* @author zsz
* @version 
* @创建时间：2019年10月29日 下午4:42:50
*/
public class FaceContrast {

	public float contrast(byte[] face1,byte[] face2,FaceEngine faceEngine) {
		 //特征比对
        FaceFeature targetFaceFeature = new FaceFeature();
        targetFaceFeature.setFeatureData(face1);
        FaceFeature sourceFaceFeature = new FaceFeature();
        sourceFaceFeature.setFeatureData(face2);
        FaceSimilar faceSimilar = new FaceSimilar();
        int compareCode = faceEngine.compareFaceFeature(targetFaceFeature, sourceFaceFeature, faceSimilar);
        //返回相似度，0-1之间
        return faceSimilar.getScore();
	}
}


